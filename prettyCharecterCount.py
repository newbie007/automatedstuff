import pprint
#pprint() and pformat()

message = "It was a cold day in april, and i was out for a ride"
count = {}

for character in message:
    count.setdefault(character, 0)
    count[character] = count[character]+1

pprint.pprint(count)
