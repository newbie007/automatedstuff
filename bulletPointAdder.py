#! /usr/bin/python3
# bulletPointAdder.py - Adds bullets point to the start
#of each line of text on clipboard

import pyperclip
text = pyperclip.paste() 
#TODO
#separate lines and add star
lines = text.split("\n")
for i in range(len(lines)):
    lines[i] = '*' + lines[i]

text = '\n'.join(lines)

pyperclip.copy(text)
