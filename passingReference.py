import copy
#copy() and deepcopy() for list that contains lists

def eggs(someParameter):
    someParameter.append('Hello')

spam = [1,3,4]
eggs(spam)
print (spam)

cats = ['loulo', 'mina', 'nounou']
copyCats = copy.copy(cats)
copyCats[1] = 'milo'
print (copyCats)
print (cats)
