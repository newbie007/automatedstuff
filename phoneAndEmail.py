#! /usr/bin/python3
import re, pyperclip

#phone regex
phoneRegex = re.compile(r'''(
        (\d{3}|\(\d{3}\))?  #Area code
        (\s|-|.)?   #Separator
        (\d{3})  #First 3digits
        (\s|-|.)
        (\d{4})  #Last 4digits
        )''', re.VERBOSE)

#email regex
emailRegex = re.compile(r'''(
        [a-zA-Z0-9]+  #First part
        @         #@ part
        (gmail|email|nostarch) 
        (.com|.fr|.ma)
        )''', re.VERBOSE)

#TODO: find matches in clipboard
text = str(pyperclip.paste())
matches = []

for groups in phoneRegex.findall(text):
    phoneNum = '-'.join([groups[1], groups[3], groups[5]])
    matches.append(phoneNum)
for groups in emailRegex.findall(text):
    matches.append(groups[0]) 

#TODO: Copy results to the clipboard 
if len(matches)>0:
    pyperclip.copy('\n'.join(matches))
    print ('Copied to clipboard:')
    print ('\n'.join(matches))
else:
    print ('No phone numbers or emails found')


