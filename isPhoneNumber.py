def isPhoneNumber(text):
    if len(text) != 12 :
        return False
    for i in range(0,3):
        if not text[i].isdecimal():
            return False
    if text[3] != '-':
        return False
    for i in range(4,7):
        if not text[i].isdecimal():
            return False
    if text[7] != '-':
        return False
    for i in range(8,12):
        if not text[i].isdecimal():
            return False
    return True

#print ('415-343-9483 is a phone number: ')
#print (isPhoneNumber('415-343-9483'))
#print ('4533-43 is a phone number: '+str(isPhoneNumber('4533-43')))

message = 'Hello, Sam, call me at 455-454-9888 personal number, and in the office 342-111-0698. bye'
for i in range(len(message)):
    chunk = message[i:i+12]
    if isPhoneNumber(chunk):
        print ('Phone number found: ' +chunk)
print ('Done')
