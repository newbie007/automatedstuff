import re, os


#TODO:read the first line and delete the sheRegex
def deleteFrom(fi, line):
    f = open(fi, 'r+')
    text = f.read()
    newText = text.replace(line, ' ')
    f.close()
    f = open(fi, 'w')
    f.write(newText)
    f.close()


#TODO:sheRegex expression
sheRegex = re.compile(r'#! .*')
pathname = os.getcwd() + '/test/python'

#TODO:loop through the filenames
for filename in os.listdir(pathname):
    fileNamePath = pathname + '/' + filename
    
    if os.path.isfile(fileNamePath):
        sheFile = open(fileNamePath, 'r+')
        if os.path.getsize(fileNamePath) > 0:
            line = sheFile.readline()
            expr = sheRegex.search(line)
            if expr == None:
                 continue
            lineToDelete = expr.group(0)
            deleteFrom(fileNamePath, lineToDelete)
        else:
            print('%s is empty' %filename)
    else:
        print('%s is not  a file' %filename)

