import bs4, requests, sys, pprint

headers2 = ['Name', 'Description', 'Language', 'Stars', 'Forks', 'Last Update']
search_url = 'https://github.com/search?q='
if len(sys.argv) > 1:

    key = ' '.join(sys.argv[1:])
    res = requests.get(search_url+key+'+')
    soup = bs4.BeautifulSoup(res.text, 'lxml')
    items = soup.findAll('li', class_='repo-list-item public source')
    menu = soup.findAll('nav', class_='menu')
    links = menu[0].findAll('a')
    for link in links:
        # text in span .counter doesn't appear all the time.
        children = link.findChildren()  

        
    myDict = {}
    
    for item in items:
        name = item.find('h3', class_='repo-list-name').text.strip()
        desc = item.find('p', class_='repo-list-description').text.strip()
        nav = item.find('div', class_='repo-list-stats').text.split()
        date = item.find('p', class_='repo-list-meta').text.split()
        lastUp = ' '.join(date)
        myDict[name] = []
        if len(nav) == 3:
            lang = nav[0]; stars = nav[1]; forks = nav[2]
        else:
            lang = 'Not specified'; stars = nav[0]; forks = nav[1]

        anotherDict = dict(zip(headers2, [name, desc, lang, stars, forks, lastUp]))
        myDict[name].append(anotherDict)
            
    # This is the dictionary that stores informations about contributors        
    #pprint.pprint(myDict)

else:
    print ('no keyword given, ex: python3 scrapGith.py css')


