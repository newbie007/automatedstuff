#! /usr/bin/python3
#pw1.py - An insecure password locker program

PASSWORDS = {'email': 'Fkgkrig324kg',
             'blog': '12945'}

import sys, pyperclip

if len(sys.argv) < 2:
    print ('Usage: Python pw.py [account] -copy account password')
    sys.exit()

account = sys.argv[1]
if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print ('Password for '+account+' copied to the clipboard')
else:
    print ('There\'s no account named ' +account)

