from selenium import webdriver
import sys, pyperclip
from time import sleep

browser = webdriver.Firefox()
browser.get('http://www.weather.gov')
textbox = browser.find_element_by_id('inputstring')

sleep(5)

if len (sys.argv) > 1:
    ameriCity = ' '.join(sys.argv[1:])
else:
    ameriCity = pyperclip.paste()

try:
    textbox.clear()
    sleep(3)
    textbox.send_keys(ameriCity)
    sleep(2)
    textbox.submit()
    sleep(8)
    temperature = browser.find_element_by_class_name('myforecast-current-sm')
    print ('The temperature of %s is %s ' %(ameriCity, temperature.text))
finally:
    browser.quit()
