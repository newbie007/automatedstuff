#for example: python3 githubProj.py css 

import requests, bs4, sys, pyperclip, os, pprint
search_url = 'https://github.com/search?q='
main_url = 'http://github.com'


os.makedirs('githubProj', exist_ok=True)

#word to search in github
if len(sys.argv) > 1:
    gitSearch = ' '.join(sys.argv[1:])
else:
    gitSearch = pyperclip.paste()

#function that return a list of data with selector 2 in a certain url
def getContent(selector1, selector2, url= search_url+gitSearch+'+'):
    content = []
    res = requests.get(url)
    soup = bs4.BeautifulSoup(res.text, 'lxml')
    listOf = soup.select(selector1)
    for i in range(len(listOf)):
        content.append(listOf[i].get(selector2))
    return content


def searchCode(aList, path):
    for item in aList:
        res = requests.get(item)
        soupe = bs4.BeautifulSoup(res.text, 'lxml')
        htmlText = soupe.select('table')
        filename = item.split('/')[-1].split('.')[0]+'.html'
        f = open(os.path.join(path,filename), 'w')
        f.write(pprint.pformat(htmlText))
        print('file: %s written' %(filename))
        f.close()


#def writeToExcel

#list of contributors
contributors = getContent('.repo-list-name a', 'href')
print(contributors)
#loop through contributors and retrieve a list of projects for each

for i in range(len(contributors)):
    #create folders
    foldername = contributors[i].split('/')[1]
    folderPath = os.path.join('githubProj', foldername)
    os.makedirs(folderPath, exist_ok=True)

    newUrl = ''.join(main_url+contributors[i])
    print ('\n\nContributor: ' ,newUrl)
    projects = getContent('.content a', 'href', newUrl)
    
#loop through subfolders and make a list of files to download
    for project in projects:
        files = []
        chunk = project.split('/')
        url = ''.join(main_url+project)
        
        #Eliminate blank space from a name
        chunk = ''.join(chunk[-1].split(' '))
        
        folders = chunk[-1].find('.')
        if folders == -1 and chunk[-1] not in ['LICENSE','source','Makefile', 'demo', 'modules']:
            #create folders
            subfolderPath = os.path.join(folderPath,chunk[-1])
            os.makedirs(subfolderPath, exist_ok=True)

            subfolders = getContent('.content a', 'href', url)
            print ('\nThis is a sub folder: ',subfolders)
            for subfolder in subfolders:
                filesInFolder = []
                newUrl = ''.join(main_url+subfolder)
                filesInFolder.append(newUrl)
                searchCode(filesInFolder, subfolderPath) 
        else:
            files.append(url)
            searchCode(files, folderPath)
          

#print (filesTo)

