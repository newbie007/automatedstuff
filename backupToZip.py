#! /usr/bin/python3
#Copies an entire folder and its contents into
#a Zip file whose name increments

import zipfile, os

def backupToZip(folder):
    #Backup the entire contents of 'folder' into a zip file

    folder = os.path.abspath(folder) #make sure folder is absolute

    #Figure out the filename this code should use based on
    #what files already exists
    number = 1
    while True:
        zipFilename = os.path.basename(folder) + '_' + str(number) + '.zip'
        if not os.path.exists(zipFilename):
            break
        number = number + 1
    
    #TODO:Create the zip file
    print ('Creating %s ..' %(zipFilename))
    backupZip = zipfile.ZipFile(zipFilename, 'w')

    #TODO:Walk the entire folder tree and compress the files in each folder
    for foldername, subfolders, filenames in os.walk(folder):
        print ('Adding files in %s ..' %(foldername))
        #Add the current folder to the Zip file
        backupZip.write(foldername)
        #Add all the files in this folder to the Zip file
        for filename in filenames:
            newBase = os.path.basename(folder) + '_'
            if filename.startswith(newBase) and filename.endswith('.zip'):
                continue #don't backup the backup Zip files
            backupZip.write(os.path.join(foldername, filename))
    backupZip.close()
        
    print ('Done')

backupToZip('walnut')
